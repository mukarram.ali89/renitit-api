import { DataTypes, Model } from "sequelize";
import { sequelize } from "../database";

export interface Geolocation {
  lat: number;
  lng: number;
  title?: string;
}

export interface ApartmentAttributes {
  id: number;
  areaSize: number;
  available: boolean;
  dateAdded: Date;
  description: string;
  floor: number;
  geolocation: Geolocation;
  name: string;
  numberOfRooms: number;
  pricePerMonth: number;
  realtorId: number;
}

export interface ApartmentCreationParams
  extends Omit<ApartmentAttributes, "id" | "available" | "dateAdded"> {}

export interface ApartmentUpdationParams
  extends Partial<Omit<ApartmentAttributes, "id">> {}

export interface ApartmentInstance extends ApartmentAttributes, Model {}

export const Apartment = sequelize.define<ApartmentInstance>(
  "Apartment",
  {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.STRING,
    },
    floor: {
      type: DataTypes.INTEGER,
    },
    areaSize: {
      type: DataTypes.INTEGER,
    },
    pricePerMonth: {
      type: DataTypes.INTEGER,
    },
    numberOfRooms: {
      type: DataTypes.INTEGER,
    },
    geolocation: {
      type: DataTypes.JSONB,
    },
    dateAdded: {
      type: DataTypes.DATE,
    },
    realtorId: {
      type: DataTypes.INTEGER,
    },
    available: {
      type: DataTypes.BOOLEAN,
    },
  },
  {
    tableName: "apartments",
  },
);

Apartment.sync({});
