import { Geolocation } from "./apartment";

export const getPointFromCoordinates = (
  lat: number,
  lng: number
): Geolocation => ({
  lat,
  lng,
});
