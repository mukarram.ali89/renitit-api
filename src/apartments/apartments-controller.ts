import {
  Body,
  Controller,
  Delete,
  Get,
  Patch,
  Path,
  Post,
  Route,
  Security,
  SuccessResponse,
  Tags,
} from "tsoa";
import { ApartmentCreationParams, ApartmentUpdationParams } from "./apartment";
import { ApartmentsService } from "./apartments-service";

@Route("apartments")
@Tags("Apartment")
export class ApartmentsController extends Controller {
  private apartmentsService: ApartmentsService;
  constructor() {
    super();
    this.apartmentsService = new ApartmentsService();
  }

  @SuccessResponse("201", "Created")
  @Post()
  @Security("jwt", ["ApartmentWrite"])
  public async createApartment(@Body() requestBody: ApartmentCreationParams) {
    this.setStatus(201);
    return await this.apartmentsService.create(requestBody);
  }

  @Delete("{apartmentId}")
  @Security("jwt", ["ApartmentWrite"])
  public async deleteApartment(@Path() apartmentId: number) {
    return await this.apartmentsService.delete(apartmentId);
  }

  @Get("{apartmentId}")
  public async getApartment(@Path() apartmentId: number) {
    return this.apartmentsService.get(apartmentId);
  }

  @Get()
  public async getApartments() {
    return this.apartmentsService.getAll();
  }

  @SuccessResponse("201", "Updated")
  @Patch("{apartmentId}")
  @Security("jwt", ["ApartmentWrite"])
  public async updateApartment(
    @Path() apartmentId: number,
    @Body() requestBody: ApartmentUpdationParams
  ) {
    const apartment = await this.apartmentsService.get(apartmentId);
    if (!apartment?.id) {
      this.setStatus(404);
      throw "Apartment doesn't exists!";
    }
    try {
      await this.apartmentsService.update(apartmentId, requestBody);
    } catch (error) {
      this.setStatus(405);
    }
    this.setStatus(200);
    return requestBody;
  }
}
