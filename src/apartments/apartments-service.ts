import {
  Apartment,
  ApartmentCreationParams,
  ApartmentUpdationParams
} from "./apartment";

export class ApartmentsService {
  public async create(apartmentCreationParams: ApartmentCreationParams) {
    return Apartment.create({
      id: Math.floor(Math.random() * 10000),
      ...apartmentCreationParams,
      available: true,
      dateAdded: new Date(),
    });
  }

  public async delete(id: number) {
    return await Apartment.destroy({ where: { id } });
  }

  public async get(id: number) {
    return await Apartment.findByPk(id);
  }

  public async getAll() {
    return await Apartment.findAll();
  }

  public async update(
    id: number,
    apartmentUpdationParams: ApartmentUpdationParams
  ) {
    return await Apartment.update(apartmentUpdationParams, {
      where: { id },
    });
  }
}
