export interface Exception extends Error {
  status: number;
}
export class UnauthorizedError extends Error implements Exception {
  status = 401;
  UnauthorizedError(message: string) {
    this.message = message;
  }
}
