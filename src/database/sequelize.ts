import { Sequelize } from "sequelize";
import { ENV } from "../shared";

export const sequelize = new Sequelize(ENV.DATABASE_URL, process.env.LOCAL ? {
  dialect: "postgres",
} : {
  dialect: "postgres",
  dialectOptions: {
    ssl: {
      rejectUnauthorized: false,
    },
  },
});
