import { generateHash, User, UserCreationParams, uuid } from "../users";

export interface UserRegisterationParams
  extends Omit<UserCreationParams, "role"> {
  password: string;
}

function isValidString(key: any) {
  return key !== undefined && typeof key === "string" && key.length > 0;
}

export function filterInvalidField(data: UserRegisterationParams) {
  const { password, email, name } = data;
  if (!isValidString(password)) {
    return "Password";
  }
  if (!isValidString(name)) {
    return "Name";
  }
  if (!isValidString(email) || !/\S+@\S+\.\S+/.test(email)) {
    return "Email";
  }
  return null;
}

export class RegisterService {
  public async register(
    userCreationParams: UserCreationParams,
    password: string,
  ) {
    const hashedPassword = generateHash(password);
    return await User.create({
      id: uuid(),
      ...userCreationParams,
      hashedPassword,
    });
  }
}
