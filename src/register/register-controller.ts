import { Body, Controller, Post, Route, Tags } from "@tsoa/runtime";
import { LoginResponse, LoginService } from "../login";
import { User } from "../users";
import {
  filterInvalidField,
  RegisterService,
  UserRegisterationParams,
} from "./register-service";

@Route("register")
@Tags("Register")
export class RegisterController extends Controller {
  private registerService: RegisterService;
  private loginService: LoginService;
  constructor() {
    super();
    this.registerService = new RegisterService();
    this.loginService = new LoginService();
  }

  @Post()
  public async register(
    @Body() data: UserRegisterationParams,
  ): Promise<LoginResponse> {
    const invalidField = filterInvalidField(data);
    if (invalidField) {
      this.setStatus(401);
      throw new Error(`${invalidField} is not valid!`);
    }

    const { password, email, name } = data;

    const existingUser = await User.findOne({ where: { email } });
    if (existingUser?.email) {
      this.setStatus(401);
      throw new Error("User already exists!");
    }

    const user = await this.registerService.register(
      {
        email,
        name,
        role: "client",
      },
      password,
    );
    const token = await this.loginService.generateToken(user);
    this.setStatus(201);
    return {
      token,
      user: {
        email: user.email,
        id: user.id,
        name: user.name,
        role: user.role,
      },
    };
  }
}
