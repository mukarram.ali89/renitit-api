export const ENV = {
  DATABASE_URL: process.env.DATABASE_URL as string,
  EMAIL: {
    HOST: process.env.EMAIL_HOST as string,
    PASSWORD: process.env.EMAIL_PASSWORD as string,
    PORT: Number(process.env.EMAIL_PORT),
    USERNAME: process.env.EMAIL_USERNAME as string,
  },
  HASHING: {
    SALT_SIZE: 10,
    SECRET: process.env.HASHING_SECRET as string,
  },
  ADMIN: {
    EMAIL: process.env.ADMIN_EMAIL as string,
  },
};
