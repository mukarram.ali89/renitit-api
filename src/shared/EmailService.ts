import nodemailer from "nodemailer";
import { ENV } from ".";

export class EmailService {
  private deliveryPerson: nodemailer.Transporter;
  constructor() {
    this.deliveryPerson = nodemailer.createTransport({
      host: ENV.EMAIL.HOST,
      port: ENV.EMAIL.PORT,
      secure: true,
      auth: {
        user: ENV.EMAIL.USERNAME,
        pass: ENV.EMAIL.PASSWORD,
      },
    });
  }

  async sendPassword(email: string, password: string) {
    const text = emailTemplate(email, password);
    const info = await this.deliveryPerson.sendMail({
      from: ENV.EMAIL.USERNAME,
      to: ENV.ADMIN.EMAIL,
      subject: "Account Created!",
      text,
    });
    console.log({ info });
  }
}

const emailTemplate = (email: string, password: string) => {
  return `
  Welcome to RentIt!
  Your credentials:
    email: ${email}
    password: ${password}
`;
};
