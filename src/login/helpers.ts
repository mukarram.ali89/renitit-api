import crypto from "crypto";

export function generateToken(): Promise<string> {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(10, (err, buffer) => {
      if (err) {
        reject(err);
      } else {
        resolve(buffer.toString("base64"));
      }
    });
  });
}
