import jwt from "jsonwebtoken";
import { ENV } from "../shared";
import { isPasswordValid, UserInstance } from "../users";

export interface Credentials {
  email: string;
  password: string;
}

export class LoginService {
  public async verify(password: string, savedHashedPassword: string) {
    return isPasswordValid(password, savedHashedPassword);
  }

  public async generateToken(user: UserInstance): Promise<string> {
    return jwt.sign(
      { email: user.email, role: user.role, id: user.id },
      ENV.HASHING.SECRET
    );
  }
}
