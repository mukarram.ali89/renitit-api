import { Body, Controller, Post, Route, Tags } from "@tsoa/runtime";
import { User, UserAttributes } from "../users";
import { Credentials, LoginService } from "./login-service";

export interface LoginResponse {
  token: string | null;
  user: Omit<UserAttributes, "hashedPassword">;
}

@Route("login")
@Tags("Login")
export class LoginController extends Controller {
  private loginService: LoginService;
  constructor() {
    super();
    this.loginService = new LoginService();
  }

  @Post()
  public async login(@Body() credentials: Credentials): Promise<LoginResponse> {
    const { password, email } = credentials;
    const user = await User.findOne({ where: { email } });
    if (!user?.email) {
      this.setStatus(401);
      throw new Error("User doesn't exists!");
    }
    const isValid = await this.loginService.verify(
      password,
      user?.hashedPassword as string,
    );
    if (!isValid) {
      this.setStatus(401);
      throw new Error("Wrong Credentials!");
    }
    const token = await this.loginService.generateToken(user);
    return {
      token,
      user: {
        email: user.email,
        id: user.id,
        name: user.name,
        role: user.role,
      },
    };
  }
}
