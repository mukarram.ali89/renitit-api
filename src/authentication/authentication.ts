import express from "express";
import jwt from "jsonwebtoken";
import { UserAttributes } from "src/users";
import { UnauthorizedError } from "../errors";
import { ENV } from "../shared";
import { Scope, scopeAllowed } from "./scopes";

function parseBearerToken(authHeader: string): string {
  return authHeader.split(" ")[1];
}

type PartialUser = Pick<UserAttributes, "email" | "role" | "id">;

async function verifyRequest(
  token: string
  // eslint-disable-next-line @typescript-eslint/ban-types
): Promise<PartialUser> {
  if (!token) {
    throw new Error("No token provided");
  }
  const decoded = jwt.verify(
    token,
    ENV.HASHING.SECRET
  ) as unknown as PartialUser;
  return Promise.resolve(decoded);
}

export async function expressAuthentication(
  request: express.Request,
  securityName: string,
  scopes: string[]
  // eslint-disable-next-line @typescript-eslint/ban-types
): Promise<object | string> {
  try {
    if (securityName === "jwt") {
      const token =
        (request.headers["x-access-token"] as string) ||
        parseBearerToken(request.headers.authorization || "");
      const { role } = await verifyRequest(token);
      for (const scope of scopes) {
        if (!scopeAllowed(role, scope as Scope)) {
          return Promise.reject(
            new UnauthorizedError(`Permission for ${scope} missing!`)
          );
        }
      }
    }
    return Promise.resolve({});
  } catch (error) {
    return Promise.reject(new UnauthorizedError(JSON.stringify(error)));
  }
}
