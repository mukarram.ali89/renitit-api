import { roles } from "src/users";

export type Scope = "UserWrite" | "UserRead" | "ApartmentWrite";

export const scopes: Record<string, Scope[]> = {
  admin: ["UserWrite", "UserRead", "ApartmentWrite"],
  realtor: ["ApartmentWrite"],
  client: [],
};

export function scopeAllowed(role: roles, scope: Scope) {
  return scopes[role].indexOf(scope) >= 0;
}
