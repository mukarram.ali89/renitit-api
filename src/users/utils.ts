import bcrypt from "bcrypt";
import { ENV } from "../shared";

export const generateHash = function (password: string) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(ENV.HASHING.SALT_SIZE));
};

export const isPasswordValid = function (
  hashedPassword: string,
  savedHashedPassword: string | undefined
) {
  if (!savedHashedPassword || !hashedPassword) {
    return false;
  }
  return bcrypt.compareSync(hashedPassword, savedHashedPassword);
};

export const uuid = function () {
  return Math.floor(Math.random() * 10000);
};
