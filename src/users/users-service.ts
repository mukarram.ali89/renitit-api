import { FindAttributeOptions } from "sequelize/types";
import { uuid } from ".";
import { User, UserCreationParams } from "./user";

const GET_QUERY = {
  attributes: {
    exclude: ["hashedPassword"],
  } as FindAttributeOptions,
};

export class UsersService {
  public async create(userCreationParams: UserCreationParams) {
    return await User.create({
      id: uuid(),
      ...userCreationParams,
    });
  }

  public async delete(id: number) {
    return await User.destroy({ where: { id } });
  }

  public async get(id: number) {
    return await User.findByPk(id, {
      ...GET_QUERY,
    });
  }

  public async getAll() {
    return await User.findAll({
      ...GET_QUERY,
    });
  }

  public async update(id: number, userUpdationParams: UserCreationParams) {
    return await User.update(userUpdationParams, {
      where: { id },
    });
  }
}
