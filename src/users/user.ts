import { DataTypes, Model } from "sequelize";
import { generateHash } from ".";
import { sequelize } from "../database";
import { generateToken } from "../login/helpers";
import { EmailService } from "../shared";

export type roles = "client" | "admin" | "realtor";

export interface UserAttributes {
  id: number;
  email: string;
  name: string;
  role: roles;
  hashedPassword: string;
}

export interface UserCreationParams
  extends Omit<UserAttributes, "id" | "hashedPassword"> {}

export interface UserInstance extends UserAttributes, Model {}

export const User = sequelize.define<UserInstance>(
  "User",
  {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    role: {
      type: DataTypes.ENUM,
      values: ["client", "realtor", "admin"],
    },
    hashedPassword: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  },
  {
    tableName: "users",
    hooks: {
      beforeCreate: async (user: UserInstance, _) => {
        if (!user.hashedPassword) {
          const password = await generateToken();
          const hashedPassword = generateHash(password);
          user.hashedPassword = hashedPassword;
          new EmailService().sendPassword(user.email, password);
        }
      },
    },
  }
);

User.sync({});
