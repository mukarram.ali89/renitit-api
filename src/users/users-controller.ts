import {
  Body,
  Controller,
  Delete,
  Get,
  Patch,
  Path,
  Post,
  Route,
  Security,
  SuccessResponse,
  Tags,
} from "tsoa";
import { UserCreationParams } from "./user";
import { UsersService } from "./users-service";

@Route("users")
@Tags("User")
export class UsersController extends Controller {
  private usersService: UsersService;
  constructor() {
    super();
    this.usersService = new UsersService();
  }

  @SuccessResponse("201", "Created")
  @Post()
  @Security("jwt", ["UserWrite"])
  public async createUser(@Body() requestBody: UserCreationParams) {
    this.setStatus(201);
    const user = await this.usersService.create(requestBody);
    return {
      id: user.id,
    };
  }

  @Delete("{userId}")
  @Security("jwt", ["UserWrite"])
  public async deleteUser(@Path() userId: number) {
    return await this.usersService.delete(userId);
  }

  @Get("{userId}")
  @Security("jwt", ["UserRead"])
  public async getUser(@Path() userId: number) {
    return this.usersService.get(userId);
  }

  @Get()
  @Security("jwt", ["UserRead"])
  public async getUsers() {
    return this.usersService.getAll();
  }

  @SuccessResponse("201", "Updated")
  @Patch("{userId}")
  @Security("jwt", ["UserWrite"])
  public async updateUser(
    @Path() userId: number,
    @Body() requestBody: UserCreationParams
  ) {
    const user = await this.usersService.get(userId);
    if (!user?.id) {
      this.setStatus(404);
      throw "User doesn't exists!";
    }
    try {
      await this.usersService.update(userId, requestBody);
    } catch (error) {
      this.setStatus(405);
    }
    this.setStatus(200);
    return requestBody;
  }
}
