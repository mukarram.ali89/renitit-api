# RentIt

[Check it out live!](https://rentitapartments.herokuapp.com/docs/)

## Development

1. Add `DATABASE_URL=<DB uri from your DB server>` in a new `.env` file

2. `yarn && yarn dev`

## Deployment

1. Add `DATABASE_URL` environment in your pipeline. Or as config vars if using heroku.

2. Merge your code into main branch. And push to heroku [remote](https://git.heroku.com/rentitapartments.git)
